def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num_times = 2)
  repeated_words = []
  num_times.times { repeated_words << word }
  repeated_words.join(" ")
end

def start_of_word(word, num_letters)
  letters = []
  word.each_char do |letter|
    letters << letter unless letters.length == num_letters
  end
  letters.join
end

def first_word(words)
  words.split.first.to_s
end

def titleize(words)
  little_words = ["and", "the", "a", "an", "the", "for", "nor", "but",
  "or", "yet", "so", "at", "around", "by", "after", "along", "from",
  "of", "on", "to", "with", "without", "over"]

  title = []
  words.split.each do |word|
    if little_words.include?(word)
      new_word = word
    else
      new_word = word.capitalize
    end
    title << new_word
  end

  title.first.capitalize!
  title.join(" ")
end
